<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('home.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">
<!--
<header class="row">
    <div class="header-image"><?= $this->Html->image('cake.logo.svg') ?></div>
    <div class="header-title">
        <h1>Welcome to CakePHP <?= Configure::version() ?> Red Velvet. Build fast. Grow solid.</h1>
    </div>
-->
</header>

<?= $this->Html->image('books.jpg'); ?>
<div class="hero">
    <h1 class="overlay">Book Author Database
    <br/>
      <span class="small"> Hello and welcome the Book Author Database.
      Here you can input and find authors and books. You may enter any 
      book you like as long as you have the book’s title, genre, author and their birthdate.</span>
      </span>
    <br/>
      <button><a href="http://fws.cal.msu.edu:8080/~ochoacin/book_db/authors">enter here<a/></button>
    </h1>
</div>

<div class="steps">
    <h1>10 Step Guide:</h1>
      <ul class="step-list">
        <li>1. Gather information needed (book title, book genre, book author and their birthdate and gender)</li>
 	<li>2. Go to Author Page</li>
        <li>3. Click on New Author</li>
        <li>4. Input needed information into correct fields</li>
        <li>5. Click submit</li>
        <li>6. Write down Author ID</li> 
        <li>7. Go to Book Page</li>
        <li>8. Click on New Book</li>
        <li>9. Input needed information into correct fields</li>
        <li>10. Click submit</li>
     </ul>
</div>
 
</body>
</html>
