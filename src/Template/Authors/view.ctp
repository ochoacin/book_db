<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Author'), ['action' => 'edit', $author->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Author'), ['action' => 'delete', $author->id], ['confirm' => __('Are you sure you want to delete # {0}?', $author->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Books'), ['controller'=>'Books', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Book'), ['controller'=>'Books', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Authors'), ['controller' => 'Authors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="authors view large-9 medium-8 columns content">
 <h3><?= h($author->name) ?></h3>
      <table id="authors_table">
           <tr>
               <th>Author ID</th>
               <td><?= $author->id ?></td>
           </tr>
           <tr>
               <th scope="row">Gender</th>
               <td scope="row"><?=$author->gender ?></td>
           </tr>
           <tr>
              <th scope="row">Birth Date</th>
              <td><?= $author->birthDate ?></td>
          </tr>
      </table>
 </div>
