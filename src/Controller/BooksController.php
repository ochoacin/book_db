<?php
// src/Controller/ArticlesController.php

namespace App\Controller;

use App\Controller\AppController;

class BooksController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }

    public function index()
    {
        $this->loadComponent('Paginator');
	$this->paginate = [
		'contain'=>['Authors']
	];
        $books = $this->Paginator->paginate($this->Books->find());
        $this->set(compact('books'));
    }

    public function view($id = null)
    {
        $book = $this->Books->findById($id)->firstOrFail();
        $this->set(compact('book'));
    }

    public function add()
    {
        $book = $this->Books->newEntity();
	$authors = $this->Books->Authors->find('list');

        if ($this->request->is('post')) {
            $book = $this->Books->patchEntity($book, $this->request->getData(), ['associated'=>['Authors']]);

            if ($this->Books->save($book)) {
                $this->Flash->success(__('Your book has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your book.'));
        }
        $this->set('book', $book);
	$this->set('authors',$authors);
    }

   public function edit($id)
   {
        $book = $this->Books->findById($id)->firstOrFail();
        if ($this->request->is(['post', 'put'])) {
        $this->Books->patchEntity($book, $this->request->getData());
        if ($this->Books->save($book)) {
            $this->Flash->success(__('Your book has been updated.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Unable to update your book.'));
    }

    $this->set('book', $book);
  }

  public function delete($id)
  {
    $this->request->allowMethod(['post', 'delete']);

    $book = $this->Books->findById($id)->firstOrFail();
    if ($this->Books->delete($book)) {
        $this->Flash->success(__('The {0} book has been deleted.', $book->title));
        return $this->redirect(['action' => 'index']);
    }
  }
}

