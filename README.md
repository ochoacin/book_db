# Book Database Application

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

An application for adding book and authors to a book database with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [book_db](https://ochoacin@gitlab.msu.edu/ochoacin/book_db.git).

## Installation
(Disclaimer: You must be on MSU Campus in order to have access to the server.)

Do the following two steps if you do not have composer installed on your computer. 
1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
Follow the commands on the website

## Run the following git commands on your terminal: 
1. cd ~/public_html/
2. git clone git@gitlab.msu.edu:ochoacin/book_db.git
4. cd book_db (This commands get you in the book_db directory)
5. composer install (Downloads all packages associated with the application)
6. Wait for the install to finish (answer yes to any questions)
7. Run the create_user_schema command to create a schema for the project
8. edit the config/app.php with the correct database credentials. (You will need to input
your username and password for the DB credentials as well as add the database name)
9. Last thing you need to do is run the following command: bin/cake migrations migrate

