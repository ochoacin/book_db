<?php

use Phinx\Migration\AbstractMigration;

class AddGendertoAuthor extends AbstractMigration
{
	public function up()
	{
		$table =  $this->table('authors', ['signed'=>false]);
		$table->addColumn('gender', 'string')->save();
	}

	public function down()
	{
		//Nothing to do here
	}

}
